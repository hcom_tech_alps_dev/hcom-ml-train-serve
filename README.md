# Train

The train module includes a class `com.hotels.ml.training.ModelTrainer`. Run this class to train 
a `org.apache.spark.ml.regression.LinearRegressionModel` using the data set in the class. The `ModelTrainer` will then
serialise the model into an [MLeap](http://mleap-docs.combust.ml/) bundle which we will use to serve the model later.

# Serve

The serve module includes a sample [Spring Boot](https://spring.io/projects/spring-boot) application serving the model
trained above. 

Run the `com.hotels.ml.service.SpringBootService` class to start the service. The service will be available at `http://localhost:8080`

To find the prediction of the test score based on the number of hours a student has studied execute:

`curl http://localhost:8080/predict?hoursStudied=4.5`

# Legal
This project is available under the [Apache 2.0 License](http://www.apache.org/licenses/LICENSE-2.0.html).

Copyright 2016-2018 Expedia Inc.
