/**
 * Copyright (C) 2016-2018 Expedia Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.hotels.ml.service.service;

import java.util.Collections;

import ml.combust.bundle.dsl.Bundle;
import ml.combust.mleap.runtime.frame.ArrayRow;
import ml.combust.mleap.runtime.frame.DefaultLeapFrame;
import ml.combust.mleap.runtime.frame.Transformer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import scala.collection.JavaConverters;
import scala.collection.Seq;
import scala.util.Try;

@Service
public class PredictionService {

    private static final Logger logger = LoggerFactory.getLogger(PredictionService.class);

    private static final Seq<String> PREDICTION_SEQ = JavaConverters.collectionAsScalaIterableConverter(Collections.singletonList("prediction")).asScala().toSeq();

    @Autowired
    private Bundle<Transformer> transformerBundle;

    public Double predict(Double hoursStudied) {
        DefaultLeapFrame leapFrame = getLeapFrame(hoursStudied);
        return extractPrediction(transformerBundle.root().transform(leapFrame));
    }

    private DefaultLeapFrame getLeapFrame(Double hoursStudied) {
        return new DefaultLeapFrame(transformerBundle.root().inputSchema(), Collections.singletonList(new ArrayRow(Collections.singletonList(hoursStudied))));
    }

    private Double extractPrediction(Try<DefaultLeapFrame> transformedLeapFrame) {
        if (transformedLeapFrame.isSuccess()) {
            Try<DefaultLeapFrame> prediction = transformedLeapFrame.get().select(PREDICTION_SEQ);
            if (prediction.isSuccess()) {
                return prediction.get().dataset().apply(0).getDouble(0);
            } else {
                logger.error("Failed to extract prediction", prediction.failed().get());
            }
        } else {
            logger.error("Failed to execute model", transformedLeapFrame.failed().get());
        }

        return 0.0D;
    }
}
