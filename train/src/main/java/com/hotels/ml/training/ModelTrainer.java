/**
 * Copyright (C) 2016-2018 Expedia Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.hotels.ml.training;

import java.io.File;
import java.util.ArrayList;

import com.google.common.collect.Lists;
import ml.combust.mleap.spark.SimpleSparkSerializer;
import org.apache.spark.ml.Pipeline;
import org.apache.spark.ml.PipelineModel;
import org.apache.spark.ml.PipelineStage;
import org.apache.spark.ml.feature.VectorAssembler;
import org.apache.spark.ml.linalg.Vectors;
import org.apache.spark.ml.regression.LinearRegression;
import org.apache.spark.ml.regression.LinearRegressionModel;
import org.apache.spark.ml.regression.LinearRegressionTrainingSummary;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ModelTrainer {

    private static final Logger logger = LoggerFactory.getLogger(ModelTrainer.class);

    private static final String BUNDLE_FILE_NAME = "mleap-bundle.zip";

    public static void main(String[] args) {
        ModelTrainer modelTrainer = new ModelTrainer();
        modelTrainer.trainModel();
    }

    private void trainModel() {
        SparkSession sparkSession = SparkSession.builder()
                .appName("Model Trainer")
                .master("local")
                .getOrCreate();

        Dataset<Row> dataset = getDataSet(sparkSession);
        PipelineModel pipelineModel = trainModel(dataset);
        printCoefficientsAndSummary(pipelineModel);

        serialiseMleapBundle(pipelineModel, dataset, BUNDLE_FILE_NAME);

        sparkSession.stop();
    }

    private Dataset<Row> getDataSet(SparkSession sparkSession) {
        ArrayList<Row> rows = Lists.newArrayList(RowFactory.create(50, 0.0),
                RowFactory.create(60, 1.0),
                RowFactory.create(70, 2.0),
                RowFactory.create(80, 3.0),
                RowFactory.create(90, 4.0),
                RowFactory.create(100, 5.0));

        StructType schema = DataTypes.createStructType(new StructField[]{
                DataTypes.createStructField("score", DataTypes.IntegerType, false),
                DataTypes.createStructField("hours", DataTypes.DoubleType, false)
        });

        return sparkSession.createDataFrame(rows, schema);
    }

    private PipelineModel trainModel(Dataset<Row> dataset) {
        VectorAssembler vectorAssembler = new VectorAssembler()
                .setInputCols(new String[] {"hours"})
                .setOutputCol("features");

        LinearRegression linearRegression = new LinearRegression().setLabelCol("score");

        Pipeline pipeline = new Pipeline().setStages(new PipelineStage[] {vectorAssembler, linearRegression});
        return pipeline.fit(dataset);
    }

    private void printCoefficientsAndSummary(PipelineModel pipelineModel) {
        LinearRegressionModel linearRegressionModel = (LinearRegressionModel) pipelineModel.stages()[pipelineModel.stages().length - 1];

        // Print the coefficients and intercept for linear regression.
        logger.info("Coefficients: {} Intercept: {}", linearRegressionModel.coefficients(), linearRegressionModel.intercept());

        // Summarize the model over the training set and print out some metrics.
        LinearRegressionTrainingSummary linearRegressionTrainingSummary = linearRegressionModel.summary();
        logger.info("numIterations: {}", linearRegressionTrainingSummary.totalIterations());
        logger.info("objectiveHistory: {}", Vectors.dense(linearRegressionTrainingSummary.objectiveHistory()));
        logger.info("RMSE: {}", linearRegressionTrainingSummary.rootMeanSquaredError());
        logger.info("r2: {}", linearRegressionTrainingSummary.r2());
    }

    private void serialiseMleapBundle(PipelineModel pipelineModel, Dataset<Row> dataset, String fileName) {
        Dataset<Row> datasetWithPredictions = pipelineModel.transform(dataset);

        SimpleSparkSerializer simpleSparkSerializer = new SimpleSparkSerializer();
        simpleSparkSerializer.serializeToBundle(pipelineModel, "jar:file:" + new File(fileName).getAbsolutePath(), datasetWithPredictions);
    }
}
